OPENCV_VERSION=3.1.0

.PHONY: install_release install_git clean uninstall

install_release:
	./setup.sh release ${OPENCV_VERSION}

install_git:
	./setup.sh git ${OPENCV_VERSION}

clean:
	rm -rf build src/opencv-*

uninstall:
	cd build
	make uninstall
