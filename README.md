## Repository for OpenCV's extra modules

This repository is intended for development of so-called "extra" modules,
contributed functionality. New modules quite often do not have stable API,
and they are not well-tested. Thus, they shouldn't be released as a part of
official OpenCV distribution, since the library maintains binary compatibility,
and tries to provide decent performance and stability.

Official repositories

* [opencv in Github](https://github.com/opencv/opencv)

* [opencv_contrib in Github](https://github.com/opencv/opencv_contrib)

### Installation
To build and install OpenCV with extra modules in your Python Virtual Environment,
a `Makefile` is provided to automate this process. Open and edit the variables
`OPENCV_VERSION` and `VIRTUAL_ENV_PATH` according to your requirements.

#### Dependencies

* virtualenv (optional: virtualenv-wrapper)
* cmake

#### To install by downloading a tarball release package
```
make install_release
```

#### To install by cloning a git repository
```
make install_git
```
