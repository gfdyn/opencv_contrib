FluidImage
==========

A module containing filters which are substitutes for some functions from `scipy.ndimage` and `scikit-image` Python libraries.

1. percentileFilter
2. percentileFilter3D


File Structure
==============
ref: http://code.opencv.org/projects/opencv/wiki/CodingStyleGuide

 *   All the file names are written in lower case for better compatibility with both POSIX and Windows.
 *   C++ interface headers have .hpp extension
 *   Implementation files have .cpp extension
 *   The implementation is put to opencv/modules/fluidimage/src, interface is added to the header files in opencv/modules/fluidimage/include/opencv2/fluidimage.
There is no need to add the files to the module explicitly, just rerun CMake and it will add the files automatically.
 *   Sample code in C++, if any, is put to opencv/samples/cpp (with exception of gpu and ocl modules, which have dedicated sample directories), sample code in Python - to opencv/samples/python2.
 *   Documentation is put to opencv/modules/fluidimage/doc, into one of the existing files or a new file/chapter is added there. In the latter case the file name should be listed in the TOC (table of content) file opencv/modules/fluidimage/doc/fluidimage.rst
 *   Tests are put to opencv/modules/fluidimage/test. When a new test source file is added, rerun CMake. If the test needs some data, put it to the separate top-level directory. For ml module the test data is put to opencv_extra/testdata/ml subdirectory, for other modules - to opencv_extra/testdata/cv subdirectory. Please, limit size of your test data files within a few megabytes, the smaller the better. If some existing test data can be used for your tests (like lena.jpg etc.), please, re-use it.

