#!/bin/bash
#Install OpenCV inside a Python virtual environment.

#CONFIGURATION
#-------------
OPENCV_DOWNLOAD_FROM=$1  # git or release
OPENCV_VERSION=$2
OPENCV_SRC_DIR="$PWD/src"
OPENCV_BUILD_DIR="$PWD/build"
OPENCV_EXTRA_MODULES="$PWD/modules"

activate_virtualenv="source $VIRTUAL_ENV/bin/activate"
PYTHON_MAJOR_VERSION=2.7 # 2.7 or 3

#DOWNLOAD
#--------
mkdir -p $OPENCV_SRC_DIR

if  [ $OPENCV_DOWNLOAD_FROM = "release" ]
then
    OPENCV_SRC_DIR="${OPENCV_SRC_DIR}/opencv-${OPENCV_VERSION}"
    if [ ! -d $OPENCV_SRC_DIR ]; then
        if [ ! -f src/$OPENCV_VERSION.tar.gz ]; then
            RELEASE=https://github.com/opencv/opencv/archive/$OPENCV_VERSION.tar.gz
            wget -O src/$OPENCV_VERSION.tar.gz $RELEASE
        fi
        tar xvf src/$OPENCV_VERSION.tar.gz -C src
    fi
else
    OPENCV_SRC_DIR="${OPENCV_SRC_DIR}/opencv-git"
    if [ ! -d $OPENCV_SRC_DIR ]; then
        git clone https://github.com/opencv/opencv.git $OPENCV_SRC_DIR
    fi
fi

if [ $OPENCV_DOWNLOAD_FROM = "git" ]
then
    cd $OPENCV_SRC_DIR
    git checkout $OPENCV_VERSION
fi

#BUILD
#------
mkdir -p $OPENCV_BUILD_DIR
mkdir -p $VIRTUAL_ENV/local

$activate_virtualenv
pip install numpy

PYTHON_DIR="python$PYTHON_MAJOR_VERSION"
PYTHON_EXE=$(which python)
NUMPY_INC=$($PYTHON_EXE -c 'from __future__ import print_function; import numpy; print(numpy.get_include())')
echo $PYTHON_EXE $NUMPY_INC

# cd $OPENCV_SRC_DIR
# patch -p1 -N -i "$OPENCV_SRC_DIR/../5852.patch"

set -e
_cmake_coreopts=("-D CMAKE_BUILD_TYPE=Release"
	         "-D CMAKE_INSTALL_PREFIX=$VIRTUAL_ENV/local"
		)

_cmake_pythonopts=("-D PYTHON_EXECUTABLE=$PYTHON_EXE"
                   "-D PYTHON_INCLUDE_DIR=$VIRTUAL_ENV/include/$PYTHON_DIR"
  	           "-D PYTHON_PACKAGES_PATH=$VIRTUAL_ENV/lib/$PYTHON_DIR/site-packages"
	           "-D PYTHON_NUMPY_INCLUDE_DIRS=$NUMPY_INC"
	           "-D INSTALL_PYTHON_EXAMPLES=ON"
	          # "-D PYTHON_LIBRARY=$VIRTUAL_ENV/lib/$PYTHON_DIR"
	          # "-D BUILD_NEW_PYTHON_SUPPORT=ON"
		  # "-D BUILD_OPENCV_PYTHON3=OFF"
		 )

_cmake_disable=("-D BUILD_EXAMPLES=OFF"
	        "-D BUILD_TESTS=OFF"
	        "-D BUILD_PERF_TESTS=OFF"
	        "-D WITH_FFMPEG=OFF"
		"-D WITH_VTK=OFF"
		"-D ENABLE_PRECOMPILED_HEADERS=OFF"  # for gcc >= 6.0.0 opencv/issues/6517
	       )
_cmake_extraopts=('-D WITH_OPENCL=ON'
                 '-D WITH_OPENGL=ON'
                 # '-D WITH_TBB=ON'
                 # '-D WITH_XINE=ON'
                 '-D WITH_GSTREAMER=OFF'
                 '-D INSTALL_C_EXAMPLES=ON'
                 '-D CMAKE_SKIP_RPATH=ON'
                 #'-D WITH_IPP=ON'
                 #'-D INSTALL_CREATE_DISTRIB=ON'
                )

_cmake_extramodules="-D OPENCV_EXTRA_MODULES_PATH=$OPENCV_EXTRA_MODULES"
	       

cd $OPENCV_BUILD_DIR
cmake ${_cmake_coreopts[@]} \
      ${_cmake_pythonopts[@]} \
      ${_cmake_disable[@]} \
      ${_cmake_extramodules} \
      $OPENCV_SRC_DIR
      # ${_cmake_extraopts[@]} \

#INSTALL
#-------
make -j4
make install
